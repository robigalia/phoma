// Copyright (c) 2017 The Robigalia Project Developers Licensed under the Apache License, Version
// 2.0 <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your option. All files in the project
// carrying such notice may not be copied, modified, or distributed except according to those
// terms.

#![no_std]
#![feature(const_fn, thread_local)]

extern crate sel4_sys;
extern crate sel4;
extern crate serde;
extern crate ssmarshal;
extern crate phoma_abi as abi;
extern crate tempo;

use serde::{Serialize};
use serde::de::DeserializeOwned;
use sel4_sys::{seL4_MsgMaxLength, seL4_GetIPCBuffer};
use sel4::ToCap;

static SUPERVISOR_CAP: sel4::Endpoint = sel4::Endpoint::from_cap(0x1);

mod ipc;
mod supervisor;
mod service;

pub use abi::{VMRights, FirmamentObjectKind};
pub use ipc::*;
pub use supervisor::*;

#[inline(always)]
fn get_msgbuf() -> &'static mut [u8] {
    unsafe {
        core::slice::from_raw_parts_mut((&mut (&mut *seL4_GetIPCBuffer()).msg).as_mut_ptr() as *mut u8,
                                        seL4_MsgMaxLength * core::mem::size_of::<usize>())
    }
}

fn call<T: Serialize, U: DeserializeOwned>(ep: sel4::Endpoint, msg: T, opt_cap: Option<usize>) -> Result<U, Error> {
    let mut msgbuf = get_msgbuf();
    let size = ssmarshal::serialize(msgbuf, &msg)?;
    let caps = match opt_cap {
        Some(c) => {
            unsafe { (&mut *seL4_GetIPCBuffer()).caps_or_badges[0] = c };
            1
        },
        None => 0
    };
    let msginfo = ep.call(0, size as _, caps)?;
    Ok(ssmarshal::deserialize(&msgbuf[..msginfo.words_transferred() * core::mem::size_of::<usize>() as usize])?.0)
}

fn call2<T: Serialize, U: DeserializeOwned>(ep: sel4::Endpoint, msg: T, cap1: usize, cap2: Option<usize>) -> Result<U, Error> {
    let mut msgbuf = get_msgbuf();
    let size = ssmarshal::serialize(msgbuf, &msg)?;
    unsafe {
        (&mut *seL4_GetIPCBuffer()).caps_or_badges[0] = cap1;
    };
    let caps = match cap2 {
        Some(c) => {
            unsafe { (&mut *seL4_GetIPCBuffer()).caps_or_badges[1] = c };
            2
        },
        None => 1
    };

    let msginfo = ep.call(0, size as _, caps)?;
    Ok(ssmarshal::deserialize(&msgbuf[..msginfo.words_transferred() * core::mem::size_of::<usize>() as usize])?.0)
}
