// Copyright (c) 2017 The Robigalia Project Developers Licensed under the Apache License, Version
// 2.0 <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your option. All files in the project
// carrying such notice may not be copied, modified, or distributed except according to those
// terms.

use abi;
use sel4::{Endpoint, Notification, Badge};
use DataRegion;

pub struct ServerHandle {
    cptr: Endpoint,
}

impl ::sel4::ToCap for ServerHandle {
    fn to_cap(&self) -> usize {
        self.cptr.to_cap()
    }
}

pub struct Connection {
    send_ntfn: Notification,
    recv_ntfn: Notification,
}

impl Connection {
    pub fn from_abi(conn: &abi::Connection) -> Connection {
        Connection {
            send_ntfn: Notification::from_cap(conn.send_ntfn as _),
            recv_ntfn: Notification::from_cap(conn.recv_ntfn as _),
        }
    }
}
