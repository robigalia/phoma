// Copyright (c) 2017 The Robigalia Project Developers Licensed under the Apache License, Version
// 2.0 <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your option. All files in the project
// carrying such notice may not be copied, modified, or distributed except according to those
// terms.

use {sel4, ssmarshal, abi, core, tempo, call, call2, ipc};
use serde::{Deserialize, Serialize};
use sel4_sys::{seL4_MsgMaxLength, seL4_GetIPCBuffer};
use sel4::ToCap;
use core::sync::atomic::{AtomicUsize, Ordering};

static SUPERVISOR_CAP: sel4::Endpoint = sel4::Endpoint::from_cap(0x1);
#[thread_local]
static mut RECV_CPTR: AtomicUsize = ::core::sync::atomic::ATOMIC_USIZE_INIT;

pub enum Object {
    Thread(Thread),
    AddressSpace(AddressSpace),
    CapSpace(CapSpace),
    DataRegion(DataRegion),
    CapRegion(CapRegion),
    WaitTree(WaitTree),
    Notification(sel4::Notification),
    Endpoint(sel4::Endpoint)
}

#[inline(never)]
#[cold]
fn bad_unwrap() -> ! {
    panic!("Tried to unwrap an object of the incorrect type!")
}

macro_rules! unwrap_methods {
    ($($meth:ident, $obj:ident, $ty:ty),*) => {
        impl Object {
            $(pub fn $meth(self) -> $ty {
                match self {
                    Object::$obj(o) => o,
                    _ => bad_unwrap(),
                }
            })*
        }
    }
}

unwrap_methods! {
    unwrap_thread, Thread, Thread,
    unwrap_address_space, AddressSpace, AddressSpace,
    unwrap_capspace, CapSpace, CapSpace,
    unwrap_data_region, DataRegion, DataRegion,
    unwrap_cap_region, CapRegion, CapRegion,
    unwrap_wait_tree, WaitTree, WaitTree,
    unwrap_ntfn, Notification, sel4::Notification,
    unwrap_endpoint, Endpoint, sel4::Endpoint
}

pub enum Error {
    Sel4(sel4::ErrorDetails),
    Marshal(ssmarshal::Error),
    AddressSpace(abi::AddressSpaceError),
    CapSpace(abi::CapSpaceError),
    SpaceBank(abi::SpaceBankError),
    FutexWait(abi::FutexWaitResponse),
    FutexRequeue(abi::FutexRequeueResponse),
    ProtocolBreach,
    InvalidArgument,
    Timeout,
    NotReady,
    CouldNotConnect,
    UnknownSeL4Error,
}

impl From<sel4::Error> for Error {
    fn from(v: sel4::Error) -> Error {
        match v.details() {
            Some(deets) => Error::Sel4(deets),
            None => Error::UnknownSeL4Error,
        }
    }
}

impl From<ssmarshal::Error> for Error {
    fn from(v: ssmarshal::Error) -> Error {
        Error::Marshal(v)
    }
}

pub struct DataRegion {
    cptr: sel4::Endpoint
}

impl DataRegion {
    pub fn get_size(&self) -> Result<usize, Error> {
        match call(self.cptr, abi::DataRegionRequest::GetSize, None)? {
            abi::DataRegionResponse::GetSize { size } => Ok(size as usize),
            _ => Err(Error::ProtocolBreach),
        }
    }

    pub fn get_permissions(&self) -> Result<abi::VMRights, Error> {
        match call(self.cptr, abi::DataRegionRequest::GetPermissions, None)? {
            abi::DataRegionResponse::GetPermissions { rights } => Ok(rights),
            _ => Err(Error::ProtocolBreach)
        }
    }

    pub fn set_permissions(&self, new: abi::VMRights) -> Result<abi::VMRights, Error> {
        match call(self.cptr, abi::DataRegionRequest::SetPermissions { rights: new }, None)? {
            abi::DataRegionResponse::SetPermissions { new_rights } => Ok(new_rights),
            _ => Err(Error::ProtocolBreach)
        }
    }
}

pub struct AddressSpace {
    cptr: sel4::Endpoint
}

impl From<abi::AddressSpaceError> for Error {
    fn from(val: abi::AddressSpaceError) -> Error {
        Error::AddressSpace(val)
    }
}

impl AddressSpace {
    // maps with lower of reg.get_permissions() and perms for each perm bit
    pub fn map_region(&self, reg: DataRegion, perms: abi::VMRights, vaddr: Option<usize>) -> Result<usize, Error> {
        match call(self.cptr, abi::AddressSpaceRequest::MapRegion { region: 0, rights: perms, vaddr: vaddr.map(|x| x as u64) }, Some(reg.cptr.to_cap()))? {
            abi::AddressSpaceResponse::MapRegion { vaddr } => Ok(vaddr as usize),
            abi::AddressSpaceResponse::Error(e) => Err(e.into()),
            _ => Err(Error::ProtocolBreach)
        }
    }
    pub fn unmap_region(&self, reg: DataRegion) -> Result<(), Error> {
        match call(self.cptr, abi::AddressSpaceRequest::UnmapRegion { region: 0 }, Some(reg.cptr.to_cap()))? {
            abi::AddressSpaceResponse::UnmapRegion => Ok(()),
            abi::AddressSpaceResponse::Error(e) => Err(e.into()),
            _ => Err(Error::ProtocolBreach)
        }
    }

    // if no new_vaddr passed, changes perms in-place
    pub fn remap_region(&self, reg: DataRegion, new_perms: abi::VMRights, new_vaddr: Option<usize>) -> Result<usize, Error> {
        match call(self.cptr, abi::AddressSpaceRequest::RemapRegion { region: 0, new_rights: new_perms, new_vaddr: new_vaddr.map(|x| x as u64) }, Some(reg.cptr.to_cap()))? {
            abi::AddressSpaceResponse::RemapRegion { vaddr } => Ok(vaddr as usize),
            abi::AddressSpaceResponse::Error(e) => Err(e.into()),
            _ => Err(Error::ProtocolBreach)
        }
    }

    pub fn verify_region(&self, reg: DataRegion) -> Result<bool, Error> {
        match call(self.cptr, abi::AddressSpaceRequest::VerifyRegion { region: 0 }, Some(reg.cptr.to_cap()))? {
            abi::AddressSpaceResponse::VerifyRegion { is_valid } => Ok(is_valid),
            abi::AddressSpaceResponse::Error(e) => Err(e.into()),
            _ => Err(Error::ProtocolBreach)
        }
    }
}

pub struct CapRegion {
    cptr: sel4::Endpoint
}

impl CapRegion {
    pub fn get_size(&self) -> Result<usize, Error> {
        match call(self.cptr, abi::CapRegionRequest::GetSize, None)? {
            abi::CapRegionResponse::GetSize { size } => Ok(size as usize),
            _ => Err(Error::ProtocolBreach),
        }
    }
}

pub struct CapSpace {
    cptr: sel4::Endpoint
}

impl From<abi::CapSpaceError> for Error {
    fn from(val: abi::CapSpaceError) -> Error {
        Error::CapSpace(val)
    }
}

impl CapSpace {
    pub fn map_region(&self, reg: CapRegion, cptr: Option<usize>) -> Result<usize, Error> {
        match call(self.cptr, abi::CapSpaceRequest::MapRegion { region: 0, cptr: cptr.map(|x| x as u64) }, Some(reg.cptr.to_cap()))? {
            abi::CapSpaceResponse::MapRegion { cptr } => Ok(cptr as usize),
            abi::CapSpaceResponse::Error(e) => Err(e.into()),
            _ => Err(Error::ProtocolBreach)
        }
    }
    pub fn unmap_region(&self, reg: CapRegion) -> Result<(), Error> {
        match call(self.cptr, abi::CapSpaceRequest::UnmapRegion { region: 0 }, Some(reg.cptr.to_cap()))? {
            abi::CapSpaceResponse::UnmapRegion => Ok(()),
            abi::CapSpaceResponse::Error(e) => Err(e.into()),
            _ => Err(Error::ProtocolBreach)
        }
    }

    pub fn remap_region(&self, reg: CapRegion, new_cptr: Option<usize>) -> Result<usize, Error> {
        match call(self.cptr, abi::CapSpaceRequest::RemapRegion { region: 0, new_cptr: new_cptr.map(|x| x as u64) }, Some(reg.cptr.to_cap()))? {
            abi::CapSpaceResponse::RemapRegion { cptr } => Ok(cptr as usize),
            abi::CapSpaceResponse::Error(e) => Err(e.into()),
            _ => Err(Error::ProtocolBreach)
        }
    }
}

pub struct SpaceBank {
    cptr: sel4::Endpoint
}

impl From<abi::SpaceBankError> for Error {
    fn from(val: abi::SpaceBankError) -> Error {
        Error::SpaceBank(val)
    }
}

impl SpaceBank {
    pub fn remaining_quota(&self) -> Result<u64, Error> {
        match call(self.cptr, abi::SpaceBankRequest::RemainingQuota, None)? {
            abi::SpaceBankResponse::RemainingQuota { size } => Ok(size),
            _ => Err(Error::ProtocolBreach)
        }
    }

    pub fn effective_quota(&self) -> Result<u64, Error> {
        match call(self.cptr, abi::SpaceBankRequest::EffectiveQuota, None)? {
            abi::SpaceBankResponse::EffectiveQuota { size } => Ok(size),
            _ => Err(Error::ProtocolBreach)
        }
    }

    pub fn destroy(&self) -> Result<(), Error> {
        match call(self.cptr, abi::SpaceBankRequest::Destroy, None)? {
            abi::SpaceBankResponse::Destroy => Ok(()),
            _ => Err(Error::ProtocolBreach)
        }
    }

    pub fn create_child(&self, quota: u64) -> Result<SpaceBank, Error> {
        match call(self.cptr, abi::SpaceBankRequest::CreateChild { quota: quota }, None)? {
            abi::SpaceBankResponse::CreateChild { cap } => Ok(SpaceBank { cptr: sel4::Endpoint::from_cap(cap as _) }),
            abi::SpaceBankResponse::Error(e) => Err(e.into()),
            _ => Err(Error::ProtocolBreach)
        }
    }

    pub fn verify<T: ToCap>(&self, other: T) -> Result<bool, Error> {
        match call(self.cptr, abi::SpaceBankRequest::Verify { cap: other.to_cap() as _ }, None)? {
            abi::SpaceBankResponse::Verify { valid } => Ok(valid),
            _ => Err(Error::ProtocolBreach)
        }
    }

    pub fn buy_object(&self, object: abi::FirmamentObjectKind) -> Result<Object, Error> {
        match call(self.cptr, abi::SpaceBankRequest::RemainingQuota, None)? {
            abi::SpaceBankResponse::BuyObject { cap } => {
                let ep = sel4::Endpoint::from_cap(cap as _);
                Ok(match object {
                    abi::FirmamentObjectKind::Thread => Object::Thread(Thread { cptr: ep }),
                    abi::FirmamentObjectKind::AddressSpace => Object::AddressSpace(AddressSpace { cptr: ep }),
                    abi::FirmamentObjectKind::CapSpace => Object::CapSpace(CapSpace { cptr: ep }),
                    abi::FirmamentObjectKind::DataRegion { .. } => Object::DataRegion(DataRegion { cptr: ep }),
                    abi::FirmamentObjectKind::CapRegion { .. } => Object::CapRegion(CapRegion { cptr: ep }),
                    abi::FirmamentObjectKind::WaitTree => Object::WaitTree(WaitTree { cptr: ep }),
                    abi::FirmamentObjectKind::Endpoint => Object::Endpoint(ep),
                    abi::FirmamentObjectKind::Notification => Object::Notification(sel4::Notification::from_cap(cap as _)),
                })
            },
            abi::SpaceBankResponse::Error(e) => Err(e.into()),
            _ => Err(Error::ProtocolBreach)
        }
    }

    pub fn sell_object<T: ToCap>(&self, object: T) -> Result<(), Error> {
        match call(self.cptr, abi::SpaceBankRequest::SellObject { cap: object.to_cap() as _ }, None)? {
            abi::SpaceBankResponse::SellObject => Ok(()),
            abi::SpaceBankResponse::Error(e) => Err(e.into()),
            _ => Err(Error::ProtocolBreach)
        }
    }
}

pub struct WaitTree {
    cptr: sel4::Endpoint
}

pub struct Thread {
    cptr: sel4::Endpoint
}

pub struct Supervisor {
    cptr: sel4::Endpoint
}

pub enum FutexResponse {
    Wat
}

impl From<abi::FutexWaitResponse> for Error {
    fn from(val: abi::FutexWaitResponse) -> Error {
        Error::FutexWait(val)
    }
}

impl From<abi::FutexRequeueResponse> for Error {
    fn from(val: abi::FutexRequeueResponse) -> Error {
        Error::FutexRequeue(val)
    }
}

impl Supervisor {
    pub fn futex_wait(&self, addr: usize, cur_val: usize, timeout: tempo::TickDifference) -> Result<(), Error> {
        let timeout = timeout.raw_diff();
        if timeout < 0 {
            return Err(Error::InvalidArgument)
        }
        match call(self.cptr, abi::SupervisorRequest::FutexWait { addr: addr as _, cur_val: cur_val as _, timeout: timeout as _ } , None)? {
            abi::SupervisorResponse::FutexWait(Some(e)) => Err(e.into()),
            abi::SupervisorResponse::FutexWait(None) => Ok(()),
            _ => Err(Error::ProtocolBreach)
        }
    }

    pub fn futex_wake(&self, addr: usize, how_many: usize) -> Result<(), Error> {
        match call(self.cptr, abi::SupervisorRequest::FutexWake { addr: addr as _, how_many: how_many as _ } , None)? {
            abi::SupervisorResponse::FutexWake => Ok(()),
            _ => Err(Error::ProtocolBreach)
        }
    }

    pub fn futex_requeue(&self, addr: usize, wake: usize, cur_val: usize, new_futex: usize, requeue_addr: usize) -> Result<(), Error> {
        match call(self.cptr, abi::SupervisorRequest::FutexRequeue { addr: addr as _, wake: wake as _, cur_val: cur_val as _, new_futex: new_futex as _, requeue_addr: requeue_addr as _ } , None)? {
            abi::SupervisorResponse::FutexRequeue(Some(e)) => Err(e.into()),
            abi::SupervisorResponse::FutexRequeue(None) => Ok(()),
            _ => Err(Error::ProtocolBreach)
        }
    }

    pub fn yield_now(&self) {
        // don't care about the result or if it succeeded.
        let _: Result<abi::SupervisorResponse, _> = call(self.cptr, abi::SupervisorRequest::Yield, None);
    }

    pub fn current_address_space(&self) -> Result<AddressSpace, Error> {
        match call(self.cptr, abi::SupervisorRequest::CurrentAddressSpace, None)? {
            abi::SupervisorResponse::CurrentAddressSpace { cptr } => Ok(AddressSpace { cptr: sel4::Endpoint::from_cap(cptr as _) }),
            _ => Err(Error::ProtocolBreach)
        }
    }

    pub fn bound_space_bank(&self) -> Result<SpaceBank, Error> {
        match call(self.cptr, abi::SupervisorRequest::BoundSpaceBank, None)? {
            abi::SupervisorResponse::BoundSpaceBank { cptr } => Ok(SpaceBank { cptr: sel4::Endpoint::from_cap(cptr as _) }),
            _ => Err(Error::ProtocolBreach)
        }
    }

    pub fn new_recv_cptr(&self) -> Result<usize, Error> {
        match call(self.cptr, abi::SupervisorRequest::NewRecvCptr, None)? {
            abi::SupervisorResponse::NewRecvCptr { cptr } => {
                unsafe {
                    RECV_CPTR.store(cptr as _, Ordering::Release);
                }
                Ok(cptr as _)
            }
            _ => Err(Error::ProtocolBreach)
        }
    }

    pub fn badge_cap<T: ToCap>(&self, ep: T, new_badge: sel4::Badge) -> Result<usize, Error> {
        match call(self.cptr, abi::SupervisorRequest::BadgeCap { ep: 0, new_badge: new_badge.get_value() as _ }, Some(ep.to_cap()))? {
            abi::SupervisorResponse::BadgeCap { cptr } => Ok(cptr as _),
            _ => Err(Error::ProtocolBreach)
        }
    }

    fn internal_connect(&self, server: ipc::ServerHandle, reg: Option<DataRegion>, can_block: bool) -> Result<ipc::Connection, Error> {
        match call2(self.cptr, abi::SupervisorRequest::EstablishConnection { server_handle: 0, can_block: can_block }, server.to_cap(), reg.map(|x| x.to_cap()))? {
            abi::SupervisorResponse::EstablishConnection { was_ready, connection } => {
                match connection {
                    Some(ref conn) => {
                        Ok(ipc::Connection::from_abi(conn))
                    },
                    None => if was_ready {
                        Err(Error::NotReady)
                    } else {
                        Err(Error::CouldNotConnect)
                    }
                }
            },
            _ => Err(Error::ProtocolBreach),
        }
    }

    /// Connect to a server, blocking if not currently able to establish a connection.
    ///
    /// If `region` is Some, the provided `DataRegion` will be used for
    /// shared-memory communications with the server. The server might not
    /// necessarily support shared-memory communications. If it doesn't, the
    /// `region` will not be used.
    pub fn connect(&self, server: ipc::ServerHandle, region: Option<DataRegion>) -> Result<ipc::Connection, Error> {
        self.internal_connect(server, region, true)
    }

    pub fn try_connect(&self, server: ipc::ServerHandle, region: Option<DataRegion>) -> Result<ipc::Connection, Error> {
        self.internal_connect(server, region, false)
    }

    // buying processes/threads/other objects?

    // auctions? probably a space bank op

    // bulk cnode transfer?

    // wait trees

    // timers?

    // future powerbox (dependency injection/object broker) operations?
}

pub fn supervisor() -> Supervisor {
    Supervisor {
        cptr: SUPERVISOR_CAP,
    }
}

macro_rules! to_cap_impl {
    ($($name:ident),*) => {
        $(
            impl sel4::ToCap for $name {
                fn to_cap(&self) -> usize {
                    self.cptr.to_cap()
                }
            }

            impl $name {
                pub fn from_cap(ep: sel4::Endpoint) -> $name {
                    $name {
                        cptr: ep
                    }
                }
            }
        )*
    }
}

to_cap_impl!{
    Thread, AddressSpace, CapSpace, DataRegion, CapRegion, WaitTree, Supervisor
}

pub fn recv_cptr() -> usize {
    unsafe {
        RECV_CPTR.load(Ordering::Release)
    }
}
